var mockServer = require('mockserver-client');
var mockServerClient = mockServer.mockServerClient;

mockServerClient("192.168.1.139", 8088).mockAnyResponse({
    'httpRequest': {
        'method': 'POST',
        'path': '/v1/idm/user/direct-reports',
        'body': {
            "type": "JSON",
            "value": JSON.stringify({
                'idmUserId': 999
            }),
            "matchType": "ONLY_MATCHING_FIELDS"
        }
    },
    'httpResponse': {
        'statusCode': 200,
        'headers': [{
            "name": "Content-Type",
            "values": ["application/json; charset=utf-8"]
        }, {
            "name": "Cache-Control",
            "values": ["public, max-age=86400"]
        }],
        'body': JSON.stringify([{
            'idmUserId': 1,
            'firstName': 'Quinn',
            'lastName': 'Chen'
        }, {
            'idmUserId': 2,
            'firstName': 'Henry',
            'lastName': 'Huang'
        }, {
            'idmUserId': 3,
            'firstName': 'Bruce',
            'lastName': 'Hu'
        }, {
            'idmUserId': 4,
            'firstName': 'Leyo',
            'lastName': 'Guan'
        }, {
            'idmUserId': 5,
            'firstName': 'Devin',
            'lastName': 'Chen'
        }])
    },
    'times': {
        'unlimited': true
    }
});

mockServerClient("192.168.1.139", 8088).mockAnyResponse({
    'httpRequest': {
        'method': 'POST',
        'path': '/v1/idm/user/login',
        'body': {
            "type": "JSON",
            "value": JSON.stringify({
                'username': 'quinnc',
                'password': 'Qwer1234'
            }),
            "matchType": "ONLY_MATCHING_FIELDS"
        }
    },
    'httpResponse': {
        'statusCode': 200,
        'headers': [{
            "name": "Content-Type",
            "values": ["application/json; charset=utf-8"]
        }, {
            "name": "Cache-Control",
            "values": ["public, max-age=86400"]
        }],
        'body': JSON.stringify({
            'success': true,
            'oAuthToken': 'fb90c991-2b11-49e8-8a07-e3c302689c6f',
            'idmUserId': 1
        })
    },
    'times': {
        'unlimited': true
    }
});

mockServerClient("192.168.1.139", 8088).mockAnyResponse({
    'httpRequest': {
        'method': 'POST',
        'path': '/v1/idm/user/authorize',
        'body': {
            "type": "JSON",
            "value": JSON.stringify({
                'oauthToken': 'invalid',
                'requestedEndpoint': '/v1/idm/user/direct-reports'
            }),
            "matchType": "ONLY_MATCHING_FIELDS"
        }
    },
    'httpResponse': {
        'statusCode': 200,
        'headers': [{
            "name": "Content-Type",
            "values": ["application/json; charset=utf-8"]
        }, {
            "name": "Cache-Control",
            "values": ["public, max-age=86400"]
        }],
        'body': JSON.stringify({
            'success': false,
            'errorMessage': 'No Permisson'
        })
    },
    'times': {
        'unlimited': true
    }
});

mockServerClient("192.168.1.139", 8088).mockAnyResponse({
    'httpRequest': {
        'method': 'POST',
        'path': '/v1/idm/user/authorize',
        'body': {
            "type": "JSON",
            "value": JSON.stringify({
                'oauthToken': 'valid',
                'requestedEndpoint': '/v1/idm/user/direct-reports'
            }),
            "matchType": "ONLY_MATCHING_FIELDS"
        }
    },
    'httpResponse': {
        'statusCode': 200,
        'headers': [{
            "name": "Content-Type",
            "values": ["application/json; charset=utf-8"]
        }, {
            "name": "Cache-Control",
            "values": ["public, max-age=86400"]
        }],
        'body': JSON.stringify({
            'success': true,
            'idmUserId': 999
        })
    },
    'times': {
        'unlimited': true
    }
});

mockServerClient("192.168.1.139", 8088).mockAnyResponse({
    'httpRequest': {
        'method': 'GET',
        'path': '/inventory/task',
        'body': {
            "matchType": "ONLY_MATCHING_FIELDS"
        }
    },
    'httpResponse': {
        'statusCode': 200,
        'headers': [{
            "name": "Content-Type",
            "values": ["application/json; charset=utf-8"]
        }, {
            "name": "Access-Control-Allow-Origin",
            "values": ["*"]
        }, {
            "name": "Access-Control-Allow-Methods",
            "values": ["*"]
        }, {
            "name": "Access-Control-Allow-Headers",
            "values": ["*"]
        }],
        'body': JSON.stringify([{
            "id": 1,
            "assignee": "bruceh",
            "status": "NEW",
            "description": "title vizio",
            "taskType": "BLIND",
            "checkType": "NONE",
            "createdBy": "bruceh",
            "countTasks": [{
                "itemSpecId": null,
                "locationId": null,
                "title": "vizio"
            }],
            "doneBefore": "2016-03-26T00:00:00",
            "priority": 1
        }])
    },
    'times': {
        'unlimited': true
    }
});